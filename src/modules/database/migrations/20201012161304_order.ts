import * as Knex from 'knex';

export async function up(knex: Knex): Promise<any> {
  await knex.schema.createTable('Orders', table => {
    table.increments('id').primary();
    table.string('description', 250).notNullable();
    table.integer('quantity', 5).notNullable();
    table.decimal('value').notNullable();
    table.dateTime('createdDate').notNullable();
    table.dateTime('updatedDate').notNullable();
  });
}

export async function down(knex: Knex): Promise<any> {
  await knex.schema.dropTableIfExists('Orders');
}
